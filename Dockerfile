FROM node:6-alpine

COPY img ./img
COPY index.html ./
COPY style.css ./
COPY server.js ./
COPY package*.json ./
RUN npm install

CMD ["node", "server.js"]
EXPOSE 8080
